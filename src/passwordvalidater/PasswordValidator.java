/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package passwordvalidater;

import java.util.regex.Pattern;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class PasswordValidator {
private static final String REGDIGIT = "^(?=.\\d)$";
private static final String REGCHAR = "^(?=.*[@,$,+!#?^&])";
        private static final String REGCAPITAL = "^(?=.*[A-Z])";
                private static final String REGMINLENGTH = "^.{8}";
        static final Pattern PATTERN1 = Pattern.compile(REGDIGIT);
 static final Pattern PATTERN2 = Pattern.compile(REGCHAR);
 static final Pattern PATTERN3 = Pattern.compile(REGCAPITAL);
 static final Pattern PATTERN4 = Pattern.compile(REGMINLENGTH);
    
 private static boolean checkDigit(String password){ 
     return PATTERN1.matcher(password).matches();
 }
  private static boolean checkChar(String password){ 
     return PATTERN2.matcher(password).matches();
 }
   private static boolean checkCapitals(String password){
     return PATTERN3.matcher(password).matches();
 }
    private static boolean checkMinLength(String password){
     return PATTERN4.matcher(password).matches();
 }
        public static boolean isvalidPassword(String userInput){
            
return checkDigit(userInput) && checkChar(userInput) && checkDigit (userInput) && checkCapitals(userInput) && checkMinLength(userInput);
        
}
  

   
    public static void main(String[] args) {
        // TODO code application logic here
        String password = "Hello@world";
    System.out.println(PasswordValidator.isvalidPassword(password));
    
    String badpassword = "Hello";
 System.out.println(PasswordValidator.isvalidPassword(badpassword));
   }



    
    
}
